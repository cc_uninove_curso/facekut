<%-- 
    Document   : cadastro
    Created on : 22/06/2020, 00:27:39
    Author     : Eduardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Facebook Theme Demo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <!--[if lt IE 9]>
          < src="//html5shim.googlecode.com/svn/trunk/html5.js"></>
        <![endif]-->
        <link href="assets/css/facebook.css" rel="stylesheet">
    </head>
    
    <body>
        
        <div class="wrapper">
            <div class="box">
                <div class="well" style="width: 40%">
                    <form class="form" action="UsuarioController">
                         <h4>Cadastre-se</h4>
                         <div class="input-group text-center">
                         <input class="form-control input-lg" placeholder="Informe seu nome" type="text" name="nome">
                         <input class="form-control input-lg" placeholder="Informe seu e-mail" type="text" name="email">
                         <input class="form-control input-lg" placeholder="Informe sua senha" type="password" name="senha">
                         <input type="hidden" name="acao" value="cadastrar"/>
                         <span class="input-group-btn"><input class="btn btn-lg btn-primary" type="submit" value="Cadastrar"/></span>

                         </div>
                         <a href="">Cadastre-se</a>
                   </form>
                 </div>
            </div>
    </div>


        <!--post modal-->
        <div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    Update Status
              </div>
              <div class="modal-body">
                  <form class="form center-block">
                    <div class="form-group">
                      <textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?"></textarea>
                    </div>
                  </form>
              </div>
              <div class="modal-footer">
                  <div>
                  <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Post</button>
                    <ul class="pull-left list-inline"><li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li><li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li><li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li></ul>
                  </div>    
              </div>
          </div>
          </div>
        </div>
        
        < type="text/java" src="assets/js/jquery.js"></>
        < type="text/java" src="assets/js/bootstrap.js"></>
        < type="text/java">
        $(document).ready(function() {
            $('[data-toggle=offcanvas]').click(function() {
                $(this).toggleClass('visible-xs text-center');
                $(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
                $('.row-offcanvas').toggleClass('active');
                $('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
                $('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
                $('#btnShow').toggle();
            });
        });
        </>
</body>
</html>