package br.com.facekut.dao;

import br.com.facekut.entities.Entitie;
import java.util.List;

/**
 *
 * @author Eduardo
 */
public interface DaoFunctions 
{
    //Métodos Obrigatórios
    public boolean add (Entitie entitie);
    public boolean delete (int id);
    public boolean update (Entitie entitie);
    public Entitie find(int id);
    public List<Entitie> findAll();
}
