package br.com.facekut.dao;

import br.com.facekut.entities.Entitie;
import br.com.facekut.entities.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eduardo
 */
public class DaoUsuario extends Dao implements DaoFunctions
{
    @Override
    public boolean add(Entitie entitie) 
    {
        Usuario usu = (Usuario) entitie;
        String comando = "INSERT INTO usuarios "
                       + "(nome, email, senha) "
                       + "VALUES(?, ?, ?)";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setString(1, usu.getNome());
            this.stmt.setString(2, usu.getEmail());
            this.stmt.setString(3, usu.getSenha());
            this.stmt.execute();
        } 
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao inserir Usuário: " + ex.getMessage());
            return false;
        }
        
        return true;
    }

    @Override
    public boolean delete(int id) 
    {
        String comando = "DELETE FROM usuarios WHERE id = ?";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setInt(1, id);
            this.stmt.execute();
        }
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao excluir Usuário: " + ex.getMessage());
            return false;
        }
        
        return true;
    }

    @Override
    public boolean update(Entitie entitie) 
    {
        Usuario usu = (Usuario) entitie;
        String comando = "UPDATE usuarios "
                       + "SET nome = ?, email = ?, senha = ?"
                       + "WHERE id = ?";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setString(1, usu.getNome());
            this.stmt.setString(3, usu.getEmail());
            this.stmt.setString(4, usu.getSenha());
            this.stmt.setInt(5, usu.getId());
            
            //ResultSet rs = this.stmt.executeQuery();
            this.stmt.execute();
        }
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao atualizar Usuário: " + ex.getMessage());
            return false;
        }
        
        return true;
    }

    @Override
    public Entitie find(int id) 
    {
        String comando = "SELECT * FROM usuarios WHERE id = ?";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setInt(1, id);
            ResultSet rs = this.stmt.executeQuery();
            
            if(rs.next())
            {
                Usuario usu = new Usuario();
                usu.setId(rs.getInt("id"));
                usu.setNome(rs.getString("nome"));
                usu.setEmail(rs.getString("email"));
                usu.setSenha(rs.getString("senha"));
                
                return usu;
            }
        }
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao encontrar Usuário: " + ex.getMessage());
            return null;
        }
        
        return null;
    }

    @Override
    public List<Entitie> findAll() 
    {
        String comando = "SELECT * FROM usuarios";
        List<Entitie> lstUsuarios = new ArrayList<>();
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            ResultSet rs = this.stmt.executeQuery();
            while(rs.next())
            {
                Usuario usu = new Usuario();
                usu.setId(rs.getInt("id"));
                usu.setNome(rs.getString("nome"));
                usu.setEmail(rs.getString("email"));
                usu.setSenha(rs.getString("senha"));
                
                lstUsuarios.add(usu);
            }
        }
        
        catch(SQLException ex)
        {
            return null;
        }
        
        return lstUsuarios;
    }
    
    public Usuario efetuarLogin(String email, String senha)
    {
        String comando = "SELECT * FROM usuarios WHERE email = ? and senha = ?";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setString(1, email);
            this.stmt.setString(2, senha);
            ResultSet rs = this.stmt.executeQuery();
            
            if(rs.next())
            {
                Usuario usu = new Usuario();
                usu.setId(rs.getInt("id"));
                usu.setNome(rs.getString("nome"));
                usu.setEmail(rs.getString("email"));
                usu.setSenha(rs.getString("senha"));
                
                return usu;
            }
        }
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao encontrar Usuário: " + ex.getMessage());
            return null;
        }
        
        return null;
    }
    
    public Usuario findByCPF(String cpf) 
    {
        String comando = "SELECT * FROM usuarios WHERE cpf = ?";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setString(1, cpf);
            ResultSet rs = this.stmt.executeQuery();
            
            if(rs.next())
            {
                Usuario usu = new Usuario();
                usu.setId(rs.getInt("id"));
                usu.setNome(rs.getString("nome"));
                usu.setEmail(rs.getString("email"));
                usu.setSenha(rs.getString("senha"));
                
                return usu;
            }
        }
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao encontrar Usuário: " + ex.getMessage());
            return null;
        }
        return null;
    }
}
