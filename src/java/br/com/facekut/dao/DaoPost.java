package br.com.facekut.dao;

import br.com.facekut.entities.Entitie;
import br.com.facekut.entities.Post;
import br.com.facekut.entities.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eduardo
 */
public class DaoPost extends Dao implements DaoFunctions
{

    @Override
    public boolean add(Entitie entitie) 
    {
        Post post = (Post) entitie;
        String comando = "INSERT INTO posts "
                       + "(idUsuario, conteudo, qtdCurtidas) "
                       + "VALUES(?, ?, ?)";
        
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            this.stmt.setInt(1, post.getId());
            this.stmt.setString(2, post.getConteudo());
            this.stmt.setInt(3, post.getQtdCurtidas());
            this.stmt.execute();
        } 
        
        catch(SQLException ex)
        {
            System.out.println("Erro ao inserir Post: " + ex.getMessage());
            return false;
        }
        
        return true;
    }

    @Override
    public boolean delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Entitie entitie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Entitie find(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Entitie> findAll() 
    {
        String comando = "Select * from posts";
        List<Entitie> lstPosts = new ArrayList<>();
    
        try
        {
            this.conectar();
            this.stmt = this.conn.prepareStatement(comando);
            ResultSet rs = this.stmt.executeQuery();
            
            while(rs.next())
            {
                int id =  rs.getInt("id");
                int idUsuario = rs.getInt("idUsuario");
                LocalDateTime data = rs.getDate("data").toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                String conteudo = rs.getString("conteudo");
                int qtdCurtidas = rs.getInt("qtdCurtidas");
                Post post  = new Post(id, idUsuario, data, conteudo, qtdCurtidas);
                lstPosts.add(post);
            }
        }
        
        catch(SQLException ex)
        {
            return null;
        }
        
        return lstPosts;
    }
}
