package br.com.facekut.controllers;

import br.com.facekut.dao.DaoPost;
import br.com.facekut.entities.Post;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Eduardo
 */
public class PostController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        String acao = request.getParameter("acao");
        int idUsuario = 0;
        String conteudo = "";
        Post post = null;
        switch(acao)
        {
            case "postar":
                idUsuario = Integer.parseInt(request.getParameter("idusuario"));
                conteudo = request.getParameter("conteudo");

                DaoPost dPost = new DaoPost();
                post = new Post(0, idUsuario, null, conteudo, 0);

                if(dPost.add(post) == true)
                {
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }
                else
                {
                    request.setAttribute("mensagem", "Erro no cadastro");
                    request.getRequestDispatcher("confirmacaocadastro.jsp").forward(request, response);
                }
               
            break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
